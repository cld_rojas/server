#!/bin/bash
now=$(date +%d/%m/%Y' '%H:%M)
function rcon {
  /opt/minecraft/tools/mcrcon/mcrcon -H 127.0.0.1 -P 25575 -p strong-password "$1"
}
rcon "save-off"
rcon "playerbags save"
rcon "save-all"
rcon "save-on"
cd /opt/minecraft/server/
git add -A
git commit -m "Daily update $now"
git push origin master

